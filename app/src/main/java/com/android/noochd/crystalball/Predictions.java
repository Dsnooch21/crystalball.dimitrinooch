package com.android.noochd.crystalball;


public class Predictions {

    private static Predictions predictions;
    private String[] answers;
//    string[] is an array of strings


    private Predictions(){
        answers = new String[] {
            "Your wishes will come true.",
                "Your wishes will never come true.",
                "your going to eat orphans in the future",
                "Get a life"

        };
    }

     public static Predictions get(){
         if(predictions == null){
             predictions = new Predictions();
         }
         return predictions;
     }
    public String getPrediction() {
        int random =(int ) (Math. random()  *4 + 1);
        return answers[random];
        //accessing the first thing in an array (Strings[])
    }
}
